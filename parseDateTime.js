/*
  parseDateTime = new ParseDateTime()

  parseDateTime.parse("昨天下午3点")
  parseDateTime.parse("上周五晚上9点")
  parseDateTime.parse("这两个礼拜")
  parseDateTime.parse("去年中秋节")
  parseDateTime.parse("早上8-10点")
  parseDateTime.parse("下个季度")
  parseDateTime.parse("10-20年")
  parseDateTime.parse("过去的2个月")
  parseDateTime.parse("3月5号中午十二点半")
  parseDateTime.parse("下个礼拜5的下午4点一刻")
*/

const _ = require("underscore")
const moment = require("moment-timezone")
const Lunar = require("lunar-javascript").Lunar
const NlpParseBase = require("./nlpparser.js").NlpParseBase

class ParseDateTime extends NlpParseBase{
  constructor(){
    super()
    this.dateMap = {
      "大前年": {"value": -3, "type": "year"},
      "前年": {"value": -2, "type": "year"},
      "上年": {"value": -1, "type":"year"},
      "去年": {"value": -1, "type": "year"},
      "今年": {"value": 0, "type": "year"},
      "本年": {"value": 0, "type": "year"},
      "明年": {"value": 1, "type": "year"},
      "后年": {"value": 2, "type": "year"},
      "大后年": {"value": 3, "type": "year"},
      "大前个月": {"value": -3, "type": "month"},
      "前个月": {"value": -2, "type": "month"},
      "上月": {"value": -1, "type": "month"},
      "上个月": {"value": -1, "type": "month"},
      "这个月": {"value": 0, "type": "month"},
      "本月": {"value": 0, "type": "month"},
      "当月": {"value": 0, "type": "month"},
      "下个月": {"value": 1, "type": "month"},
      "下下个月": {"value": 2, "type": "month"},
      "大前天": {"value": -3, "type": "day"},
      "前天": {"value": -2, "type": "day"},
      "昨天": {"value": -1, "type": "day"},
      "昨": {"value": -1, "type": "day"},
      "今天": {"value": 0, "type": "day"},
      "当天": {"value": 0, "type": "day"},
      "今": {"value": 0, "type": "day"},
      "明天": {"value": 1, "type": "day"},
      "明": {"value": 1, "type": "day"},
      "后天": {"value": 2, "type": "day"},
      "大后天": {"value": 3, "type": "day"},
      "这季度": {"value": 0, "type": "quarter"},
      "这个季度": {"value": 0, "type": "quarter"},
      "本季度": {"value": 0, "type": "quarter"},
      "上季度": {"value": -1, "type": "quarter"},
      "上个季度": {"value": -1, "type": "quarter"},
      "下季度": {"value": 1, "type": "quarter"},
      "下个季度": {"value": 1, "type": "quarter"},
    };
    this.cnDate = _.keys(this.dateMap).join("|");
    this.weekMap = {
      "周": {"value": 0, "type": "week"},
      "星期": {"value": 0, "type": "week"},
      "礼拜": {"value": 0, "type": "week"},
      "这周": {"value": 0, "type": "week"},
      "本周": {"value": 0, "type": "week"},
      "这个礼拜": {"value": 0, "type": "week"},
      "本个星期": {"value": 0, "type": "week"},
      "上周": {"value": -1, "type": "week"},
      "上礼拜": {"value": -1, "type": "week"},
      "上星期": {"value": -1, "type": "week"},
      "上个礼拜": {"value": -1, "type": "week"},
      "上个星期": {"value": -1, "type": "week"},
      "上上周": {"value": -2, "type": "week"},
      "上上个星期": {"value": -2, "type": "week"},
      "上上个礼拜": {"value": -2, "type": "week"},
      "下周": {"value": 1, "type": "week"},
      "下星期": {"value": 1, "type": "week"},
      "下个周": {"value": 1, "type": "week"},
      "下个星期": {"value": 1, "type": "week"},
      "下礼拜": {"value": 1, "type": "week"},
      "下个礼拜": {"value": 1, "type": "week"},
      "下下周": {"value": 2, "type": "week"},
      "下下个星期": {"value": 2, "type": "week"},
      "下下个礼拜": {"value": 2, "type": "week"}
    };
    this.cnWeek = _.keys(this.weekMap).join("|");
    this.holidays = [
      {
        "name": "元旦节",
        "holi": ["元旦", "新年"],
        "date": "01.01",
        "type": "公历"
      },
      {
        "name": "妇女节",
        "holi": ["妇女", "三八", "38"],
        "date": "03.08",
        "type": "公历"
      },
      {
        "name": "植树节",
        "holi": ["植树"],
        "date": "03.12",
        "type": "公历"
      },
      {
        "name": "清明节",
        "holi": ["清明"],
        "date": "04.05",
        "type": "公历"
      },
      {
        "name": "劳动节",
        "holi": ["劳动", "五一"],
        "date": "05.01",
        "type": "公历"
      },
      {
        "name": "儿童节",
        "holi": ["儿童", "61", "六一"],
        "date": "06.01",
        "type": "公历"
      },
      {
        "name": "教师节",
        "holi": ["教师"],
        "date": "09.10",
        "type": "公历"
      },
      {
        "name": "国庆节",
        "holi": ["国庆"],
        "date": "10.01",
        "type": "公历"
      },
      {
        "name": "光棍节",
        "holi": ["光棍", "双十一", "双11"],
        "date": "11.11",
        "type": "公历"
      },
      {
        "name": "情人节",
        "holi": ["情人"],
        "date": "02.14",
        "type": "公历"
      },
      {
        "name": "愚人节",
        "holi": ["愚人"],
        "date": "04.01",
        "type": "公历"
      },
      {
        "name": "万圣节",
        "holi": ["万圣"],
        "date": "11.01",
        "type": "公历"
      },
      {
        "name": "圣诞节",
        "holi": ["圣诞"],
        "date": "12.25",
        "type": "公历"
      },
      {
        "name": "春节",
        "holi": ["春节", "大年初一"],
        "date": "01.01",
        "type": "农历"
      },
      {
        "name": "元宵节",
        "holi": ["元宵", "上元"],
        "date": "01.15",
        "type": "农历"
      },
      {
        "name": "端午节",
        "holi": ["端午"],
        "date": "05.05",
        "type": "农历"
      },
      {
        "name": "七夕节",
        "holi": ["七夕"],
        "date": "07.07",
        "type": "农历"
      },
      {
        "name": "中秋节",
        "holi": ["中秋", "仲秋", "中元"],
        "date": "08.15",
        "type": "农历"
      },
      {
        "name": "重阳节",
        "holi": ["重阳"],
        "date": "09.09",
        "type": "农历"
      },
      {
        "name": "腊八节",
        "holi": ["腊八"],
        "date": "12.08",
        "type": "农历"
      },
      {
        "name": "除夕节",
        "holi": ["除夕", "大年夜", "除夕夜"],
        "date": "12.30",
        "type": "农历"
      },
      {
        "name": "小年",
        "holi": ["小年", "小年夜"],
        "date": "12.24",
        "type": "农历"
      },
      {
        "name": "小寒",
        "holi": ["小寒"],
        "date": "01.05-07",
        "type": "节气",
        "index": 1
      },
      {
        "name": "大寒",
        "holi": ["大寒"],
        "date": "01.20-21",
        "type": "节气",
        "index": 2
      },
      {
        "name": "立春",
        "holi": ["立春"],
        "date": "02.03-05",
        "type": "节气",
        "index": 3
      },
      {
        "name": "雨水",
        "holi": ["雨水"],
        "date": "02.18-20",
        "type": "节气",
        "index": 4
      },
      {
        "name": "惊蛰",
        "holi": ["惊蛰"],
        "date": "03.05-07",
        "type": "节气",
        "index": 5
      },
      {
        "name": "春分",
        "holi": ["春分"],
        "date": "03-20-22",
        "type": "节气",
        "index": 6
      },
      {
        "name": "清明",
        "holi": ["清明"],
        "date": "04.04-06",
        "type": "节气",
        "index": 7
      },
      {
        "name": "谷雨",
        "holi": ["谷雨", "谷雨那天"],
        "date": "04.19-21",
        "type": "节气",
        "index": 8
      },
      {
        "name": "立夏",
        "holi": ["立夏"],
        "date": "05.05-07",
        "type": "节气",
        "index": 9
      },
      {
        "name": "小满",
        "holi": ["小满"],
        "date": "05.20-22",
        "type": "节气",
        "index": 10
      },
      {
        "name": "芒种",
        "holi": ["芒种"],
        "date": "06.05-07",
        "type": "节气",
        "index": 11
      },
      {
        "name": "夏至",
        "holi": ["夏至"],
        "date": "06.21-22",
        "type": "节气",
        "index": 12
      },
      {
        "name": "小暑",
        "holi": ["小暑"],
        "date": "07.06-08",
        "type": "节气",
        "index": 13
      },
      {
        "name": "大暑",
        "holi": ["大暑"],
        "date": "07.22-24",
        "type": "节气",
        "index": 14
      },
      {
        "name": "立秋",
        "holi": ["立秋"],
        "date": "08.07-09",
        "type": "节气",
        "index": 15
      },
      {
        "name": "处暑",
        "holi": ["处暑"],
        "date": "08.22-24",
        "type": "节气",
        "index": 16
      },
      {
        "name": "白露",
        "holi": ["白露"],
        "date": "09.07-09",
        "type": "节气",
        "index": 17
      },
      {
        "name": "秋分",
        "holi": ["秋分"],
        "date": "09.22-24",
        "type": "节气",
        "index": 18
      },
      {
        "name": "寒露",
        "holi": ["寒露"],
        "date": "10.08-09",
        "type": "节气",
        "index": 19
      },
      {
        "name": "霜降",
        "holi": ["霜降"],
        "date": "10.23-24",
        "type": "节气",
        "index": 20
      },
      {
        "name": "立冬",
        "holi": ["立冬"],
        "date": "11.07-08",
        "type": "节气",
        "index": 21
      },
      {
        "name": "小雪",
        "holi": ["小雪"],
        "date": "11.22-23",
        "type": "节气",
        "index": 22
      },
      {
        "name": "大雪",
        "holi": ["大雪"],
        "date": "12.06-08",
        "type": "节气",
        "index": 23
      },
      {
        "name": "冬至",
        "holi": ["冬至"],
        "date": "12.21-23",
        "type": "节气",
        "index": 24
      },
    ];
    let tempHolidays = [];
    this.holidays.forEach((x)=>
      tempHolidays.push(x["holi"])
    )
    this.cnHolidays = tempHolidays.flat().join("|");

    this.segs = [
      {
        "name": "凌晨",
        "seg": ["凌晨"],
        "stime": "04:00:00",
        "etime": "06:00:00",
      },
      {
        "name": "清晨",
        "seg": ["早晨", "清晨", "早餐", "早上", "清早", "一早", "一大早", "一大清早", "早"],
        "stime": "06:00:00",
        "etime": "08:00:00",
      },
      {
        "name": "上午",
        "seg": ["上午"],
        "stime": "08:00:00",
        "etime": "11:30:00",
      },
      {
        "name": "中午",
        "seg": ["中午", "午餐", "中餐"],
        "stime": "11:30:00",
        "etime": "14:00:00",
      },
      {
        "name": "下午",
        "seg": ["下午"],
        "stime": "14:00:00",
        "etime": "18:30:00",
      },
      {
        "name": "晚上",
        "seg": ["晚上", "傍晚", "黄昏", "晚餐", "晚"],
        "stime": "18:30:00",
        "etime": "21:30:00"
      },
      {
        "name": "夜晚",
        "seg": ["夜晚", "夜宵"],
        "stime": "21:30:00",
        "etime": "23:59:00",
      },
      {
        "name": "子夜",
        "seg": ["子夜"],
        "stime": "00:00:00",
        "etime": "01:59:00"
      },
      {
        "name": "午夜",
        "seg": ["午夜", "半夜"],
        "stime": "02:00:00",
        "etime": "03:59:00"
      },
      {
        "name": "现在",
        "seg": ["现在", "刚才", "刚刚"],
        "stime": "now",
        "etime": "now"
      },
      {
        "name": "子时",
        "seg": ["子时"],
        "stime": "23:00:00",
        "etime": "00:59:00"
      },
      {
        "name": "丑时",
        "seg": ["丑时"],
        "stime": "1:00:00",
        "etime": "2:59:00"
      },
      {
        "name": "寅时",
        "seg": ["寅时"],
        "stime": "3:00:00",
        "etime": "4:59:00"
      },
      {
        "name": "卯时",
        "seg": ["卯时"],
        "stime": "5:00:00",
        "etime": "6:59:00"
      },
      {
        "name": "辰时",
        "seg": ["辰时"],
        "stime": "7:00:00",
        "etime": "8:59:00"
      },
      {
        "name": "巳时",
        "seg": ["巳时"],
        "stime": "9:00:00",
        "etime": "10:59:00"
      },
      {
        "name": "午时",
        "seg": ["午时"],
        "stime": "11:00:00",
        "etime": "12:59:00"
      },
      {
        "name": "未时",
        "seg": ["未时"],
        "stime": "13:00:00",
        "etime": "14:59:00"
      },
      {
        "name": "申时",
        "seg": ["申时"],
        "stime": "15:00:00",
        "etime": "16:59:00"
      },
      {
        "name": "酉时",
        "seg": ["酉时"],
        "stime": "17:00:00",
        "etime": "18:59:00"
      },
      {
        "name": "戌时",
        "seg": ["戌时"],
        "stime": "19:00:00",
        "etime": "20:59:00"
      },
      {
        "name": "亥时",
        "seg": ["亥时"],
        "stime": "21:00:00",
        "etime": "22:59:00"
      }
    ];
    let tempSegs = [];
    this.segs.forEach((x)=>
      tempSegs.push(x["seg"])
    );
    this.cnSegs = tempSegs.flat().join("|");

    this.weekci = "一|二|三|四|五|六|日|天|末|[1-6]";
    this.liangci = "秒钟|秒|分钟|分|小时|钟头|天|周|礼拜|星期|月|年|刻钟|刻|字";
    this.unitsMap = {
      "秒钟": {"name": "秒", "type": "second"},
      "秒": {"name": "秒", "type": "second"},
      "分钟": {"name": "分钟", "type": "minute"},
      "分": {"name": "分钟", "type": "minute"},
      "小时": {"name": "小时", "type": "hour"},
      "钟头": {"name": "小时", "type": "hour"},
      "天": {"name": "天", "type": "day"},
      "日": {"name": "天", "type": "day"},
      "号": {"name": "天", "type": "day"},
      "周": {"name": "周", "type": "week"},
      "星期": {"name": "周", "type": "week"},
      "礼拜": {"name": "周", "type": "week"},
      "月": {"name": "月", "type": "month"},
      "季度": {"name": "季度", "type": "quarter"},
      "年": {"name": "年", "type": "year"},
      "刻钟": {"name": "刻钟", "type": "minute"},
      "刻": {"name": "刻钟", "type": "minute"},
      "字": {"name": "字", "type": "minute"},
      "点": {"name": "小时", "type": "hour"},
      ":": {"name": "小时", "type": "hour"},
      "：": {"name": "小时", "type": "hour"}
    };
    this.cnUnits = _.keys(this.unitsMap).join('|');
    this.accList = [
      "year",
      "quarter",
      "month",
      "week",
      "day",
      "hour",
      "minute",
      "second"
    ];
  }
  cn2day(str, dir) {
    let cn2numBefore = {
      "一": -6,
      "二": -5,
      "三": -4,
      "四": -3,
      "五": -2,
      "六": -1,
      "天": -7,
      "日": -7,
      "末": -2,
      "1": -6,
      "2": -5,
      "3": -4,
      "4": -3,
      "5": -2,
      "6": -1
      };
    let cn2numAfter = {
      "一": 8,
      "二": 9,
      "三": 10,
      "四": 11,
      "五": 12,
      "六": 13,
      "天": 14,
      "日": 14,
      "末": 12,
      "1": 8,
      "2": 9,
      "3": 10,
      "4": 11,
      "5": 12,
      "6": 13
      };
    let cn2numCurrent = {
      "一": 1,
      "二": 2,
      "三": 3,
      "四": 4,
      "五": 5,
      "六": 6,
      "天": 0,
      "日": 0,
      "末": 5,
      "1": 1,
      "2": 2,
      "3": 3,
      "4": 4,
      "5": 5,
      "6": 6
      };
    switch (dir) {
      case -1:
        return cn2numBefore[str];
      case 0:
        return cn2numCurrent[str];
      case 1:
        return cn2numAfter[str];
    }
  }
  dayFirstHour(time) {
    return moment(time).startOf("day").toDate()
  }

  dayLastHour(time) {
    return moment(time).endOf("day").toDate()
  }

  weekFirstDay(time) {
    return moment(time).startOf("week").toDate()
  }

  weekLastDay(time) {
    return moment(time).endOf("week").toDate()
  }

  quarterFirstDay(time) {
    let quarter = Math.floor(moment(time).month()/3) + 1
    return moment(time).month((quarter - 1) * 3).startOf("month").toDate()
  }

  quarterLastDay(time) {
    let quarter = Math.floor(moment(time).month()/3) + 1
    return moment(time).month((quarter - 1) * 3 + 2).endOf("month").toDate()
  }

  monthFirstDay(time) {
    return moment(time).startOf("month").toDate()
  }

  monthLastDay(time) {
    return moment(time).endOf("month").toDate()
  }

  yearFirstDay(time) {
    return moment(time).startOf("year").toDate()
  }

  yearLastDay(time) {
    return moment(time).endOf("year").toDate()
  }
  firstOf(time,acc) {
    switch (acc) {
      case "year":
        return this.yearFirstDay(time);
      case "quarter":
        return this.quarterFirstDay(time);
      case "month":
        return this.monthFirstDay(time);
      case "week":
        return this.weekFirstDay(time);
      case "day":
        return this.dayFirstHour(time);
      case "hour":
        return moment(time).startOf("hour").toDate()
      case "minute":
        return moment(time).startOf("minute").toDate()
      default:
        return null;
    }
  }

  lastOf(time,acc) {
    switch (acc) {
      case "year":
        return this.yearLastDay(time);
      case "quarter":
        return this.quarterLastDay(time);
      case "month":
        return this.monthLastDay(time);
      case "week":
        return this.weekLastDay(time);
      case "day":
        return this.dayLastHour(time);
      case "hour":
        return moment(time).endOf("hour").toDate()
      case "minute":
        return moment(time).endOf("minute").toDate()
      default:
        return null;
    }
  }
  add(time,value,acc) {
    switch (acc) {
      case "year":
        return moment(time).add(value,"year").toDate()
      case "quarter":
        return moment(time).add(value*3,"month").toDate()    
      case "month":
        return moment(time).add(value,"month").toDate()
      case "week":
        return moment(time).add(value,"week").toDate()
      case "day":
        return moment(time).add(value,"day").toDate()  
      case "hour":
        return moment(time).add(value,"hour").toDate()
      case "minute":
        return moment(time).add(value,"minute").toDate()
      case "second":
        return moment(time).add(value,"second").toDate()
    }
  }

  subtract(time,value,acc) {
    switch (acc) {
      case "year":
        return moment(time).subtract(value,"year").toDate()
      case "quarter":
        return moment(time).subtract(value*3,"month").toDate()
      case "month":
        return moment(time).subtract(value,"month").toDate()
      case "week":
        return moment(time).subtract(value,"week").toDate()
      case "day":
        return moment(time).subtract(value,"day").toDate()
      case "hour":
        return moment(time).subtract(value,"hour").toDate()
      case "minute":
        return moment(time).subtract(value,"minute").toDate()
      case "second":
        return moment(time).subtract(value,"second").toDate()
    }
  }

  setDateTime(type,args, last) {
    let acc;
    let metric;
    let year, month, day, hour, minute, second, week, quarter;
    let year1, month1, day1, hour1, minute1, second1, week1, quarter1;
    let isDuration = false;
    let result;
    if (last == null) {
      result = {};
      acc = "";
    } else {
      //result = Map.from(last);!!!!
      result = last
      acc = result["acc"];
    }
    if (type=="a") {
      // 最近这3个月、这两个季度、上一个月、接下来一星期
      isDuration = true;
      let dText = args[0];
      let unit = this.unitsMap[args[3]]["type"];
      let value = parseInt(this.cn2num(args[1] == "" ? "1" : args[1])??1)
      if (args[2]=="半"){
        value+=0.5
      }
      let now = new Date();
      let base, sdate, edate;
      if (dText == "上" || dText == "过去") {
        //上周是指以这周起始为基点，前推1周
        base = now;
        edate = now;
        sdate = this.firstOf(this.subtract(base, value, unit),unit);
        year = sdate.getFullYear();
        month = sdate.getMonth();
        day = sdate.getDate();
        hour = sdate.getHours();
        minute = sdate.getMinutes();
      } else if (dText == "接下来这" || dText == "接下来" || dText == "下") {
        //接下来的2个月意思是包含这个月，以这个月月末为起点后推2个月
        sdate = now;
        base = this.lastOf(sdate, unit);
        edate = this.add(base, value, unit);
        year = edate.getFullYear();
        month = edate.getMonth();
        day = edate.getDate();
        hour = edate.getHours();
        minute = edate.getMinutes();
      } else {
        //近2个月意思是包含这个月，再前推1个月，
        edate = now;
        base = this.firstOf(edate, unit);
        sdate = this.subtract(base, value - 1, unit);
        year = sdate.getFullYear();
        month = sdate.getMonth();
        day = sdate.getDate();
        hour = sdate.getHours();
        minute = sdate.getMinutes();
      }
      acc = unit;
      //print(
      //    "$unit:${DateTimeLib.format(sdate, "YYYY.MM.DD HH:mi:ss")} - ${DateTimeLib.format(edate, "YYYY.MM.DD HH:mi:ss")}");
    }
    if (type=="b") {
      // 2小时前，2个月后，1年前
      let direct = args[8];
      let value1 = this.cn2num(args[0]) ?? 0;
      let half1 = args[1] == "半" ? 0.5 : (args[3] == "半" ? 0.5 : 0)
      value1 = value1 + half1
      let value2 = this.cn2num(args[4])
      let half2 = args[5] == "半" ? 0.5 : (args[7] == "半" ? 0.5 : 0)
      value2 = value2 + half2
      let unit;
      let unit1 = args[2];
      let temp;
      let unit2 = args[6];
      if (unit2 == null) {
        //两天半
        unit2 = unit1;
        if (value2 >= 1){
           value2 = parseFloat(`0.${value2}`)
        }
      }
      unit = unit2 != null ? unit2 : unit1;
      if (direct == "前") {
        temp = moment()
        .subtract(parseInt(this.convert("分钟", value1, unit1, "秒")),"second")
        .subtract(parseInt(this.convert("分钟", value2 ?? 0, unit2 ?? "", "秒") ?? 0.0),"second")
        .toDate()
        //print(DateTimeLib.format(temp, "YYYY/MM/DD HH:MI:SS"));
        //print("?????,$value1,$unit1,$value2,$unit2");
      } else {
        temp = moment()
            .add(parseInt(this.convert("分钟", value1, unit1, "秒")),"second")
            .add(parseInt(this.convert("分钟", value2 ?? 0, unit2 ?? "", "秒") ?? 0.0) , "second")
            .toDate()
      }
      //print(DateTimeLib.format(temp, "YYYY/MM/DD HH:MI:SS"));
      if (unit == "年") {
        year = this.firstOf(temp,"year").getFullYear()
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("year")) {
          acc = "year";
        }
      } else if (unit == "月") {
        temp = this.monthFirstDay(temp);
        year = temp.getFullYear();
        month = temp.getMonth();
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("month")) {
          acc = "month";
        }
      } else if (unit == "天") {
        year = temp.getFullYear();
        month = temp.getMonth();
        day = temp.getDate();
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("day")) {
          acc = "day";
        }
      } else if (unit == "小时") {
        //两小时前不应被认为是去掉分钟数的时间
        year = temp.getFullYear();
        month = temp.getMonth();
        day = temp.getDate();
        hour = temp.getHours();
        minute = temp.getMinutes();
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("minute")) {
          acc = "minute";
        }
      } else if (unit == "分钟") {
        year = temp.getFullYear();
        month = temp.getMonth();
        day = temp.getDate();
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("minute")) {
          acc = "minute";
        }
      } else if ( ["周","礼拜","星期"].includes(unit)){
        year = temp.getFullYear();
        month = temp.getMonth();
        day = temp.getDate();
        hour = temp.getHours();
        minute = temp.getMinutes();
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("day")) {
          acc = "week";
        } 
      }
      //print("new time:${DateTimeLib.format(temp, "YYYY.MM.DD HH:mi:ss")}");
    }
    
    if (type == "c") {
      //2021年、3月、5日、3点20、15点半
      //3-5点、3到5月
      let num1 = args[0]
      let num2 = args[1]
      let unit = args[2]

      if (unit == null) {
        if (result["metric"] == "hour") {
          if (num1 == 0.5) {
            num1 = 30.0;
            unit = "分";
          } else {
            unit = "分";
          }
        }
      }

      if (unit != null) {
        switch (this.unitsMap[unit]["type"]) {
          case "year":
            if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("year")) {
              acc = "year";
            }
            year = parseInt(num1);
            if (year<100){
                if (year > 50) {
                year = 1900 + year;
                }else{
                year = 2000 + year;
                }
            }
            if (num2 != null) {
              year1 = parseInt(num2)
              if (year1<100){
                if (year1 > 50) {
                    year1 = 1900 + year1;
                }else{
                    year1 = 2000 + year1;
                }
              }
            }
            break;
          case "month":
            if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("month")) {
              acc = "month";
            }
            month = parseInt(num1)-1;
            if (num2 != null) {
              month1 = parseInt(num2)-1;
            }
            break;
          case "day":
            if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("day")) {
              acc = "day";
            }
            day = parseInt(num1);
            if (num2 != null) {
              day1 = parseInt(num2)
            }
            break;
          case "hour":
            if (unit != "小时") {
              metric = "hour";
              if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("hour")) {
                acc = "hour";
              }
              hour = parseInt(num1);
              if ((result["hour"] ?? 0) >= 12 && hour < 12) {
                hour += 12;
              }
              minute = 0;
              hour1 = hour;
              minute1 = 0;
              if (num2 != null) {
                hour1 = parseInt(num2)
                if ((result["hour"] ?? 0) >= 12 && hour1 < 12) {
                  hour1 += 12;
                }
              }
            }
            break;
          case "minute":
            if (unit != "分钟" && unit != "刻钟") {
              if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("minute")) {
                acc = "minute";
              }
              minute = parseInt(this.convert("分钟", num1 ?? 0, unit) ?? 0);
              minute1 = minute;
              if (num2 != null) {
                minute1 = parseInt(this.convert("分钟", num2, unit) ?? 0);
              }
            }
            break;
          case "week":
            //not implment
            //第3周，第4周，可能基于年的第几周，可能基于月的第几周
            if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("week")) {
              acc = "week";
            }
            break;
          case "quarter":
            //第4季度，可能基于年的第几季度
            if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("quarter")) {
              acc = "quarter";
            }
            year = result["year"] ?? moment().year();
            month = parseInt((num1 - 1) * 3 );
            if (num2 != null) {
              year1 = result["year1"] ?? moment().year();
              month1 = parseInt((num2 - 1) * 3 );
            }
            break;
        }
      }
    }
    if (type=="d"){
        //去年、这个月、上周、下个季度
        let dateText = args[0]
        if (this.dateMap[dateText] != null) {
            switch (this.dateMap[dateText]["type"]) {
            case "year":
                if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("year")) {
                  acc = "year";
                }
                year = moment().year() + parseInt(`${this.dateMap[dateText]["value"]}`);
                break;
            case "month":
                if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("month")) {
                  acc = "month";
                }
                month =
                    moment().month() + parseInt(`${this.dateMap[dateText]["value"]}`);
                break;
            case "day":
                if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("day")) {
                  acc = "day";
                }
                day = moment().date() + parseInt(`${this.dateMap[dateText]["value"]}`);
                break;
            case "week":
                break;
            case "quarter":
                if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("quarter")) {
                  acc = "quarter";
                }
                let base
                let quarterVal = parseInt(`${this.dateMap[dateText]["value"]}`);
                switch (quarterVal) {
                case 0:
                    base = this.quarterFirstDay(moment());
                    break;
                case 1:
                    base = this.add(this.quarterLastDay(moment()),1,"day")
                    break;
                case -1:
                    base = this.quarterFirstDay(
                        this.subtract(this.quarterFirstDay(moment()),1,"day"))
                    break;
                default:
                    print("have not implement!");
                }
                year = base.getFullYear();
                month = base.getMonth();
                break;
            }
        }
    }
    
    if (type == "e") {
      //上周3，下周5，这个礼拜三
      let weekText = args[0];
      let weekNum = args[1];
      if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("week")) {
        acc = "week";
      }
      let base;
      let now;
      let weekVal = parseInt(`${this.weekMap[weekText]["value"]}`);
      let onlyWeek = ["周", "星期", "礼拜"].includes(weekText);
      now = moment(
          { "year":(result["year"] == null || !onlyWeek) ? moment().year() : result["year"],
            "month":(result["month"] == null || !onlyWeek) ? moment().month() : result["month"],
            "date":(result["day"] == null || !onlyWeek) ? moment().date() : result["day"]
      })
      base = this.add(this.weekFirstDay(now),weekVal,"week")
      if (weekNum != null) {
        if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("day")) {
          acc = "day";
        }
        base = this.add(base,this.cn2day(weekNum, 0),"day");
      }
      year = base.getFullYear();
      month = base.getMonth();
      day = base.getDate();
    }
    
    if (type == "f") {
      //中秋、清明、国庆、春节
      let holidayText = args[0];
      acc = "day";
      for (let item of this.holidays) {
        if (item["holi"].includes(holidayText)) {
          if (item["type"] == "农历") {
            let temp = item["date"].split(".");
            let ld = Lunar.fromYmd(year ?? moment().year(),
                parseInt(temp[0]), parseInt(temp[1]));
            year = ld.getSolar().getYear();
            month = ld.getSolar().getMonth() - 1;
            day = ld.getSolar().getDay();
            break;
          } else if (item["type"] == "节气") {
            let d = Lunar.fromDate(DateTime(year ?? DateTime.now().year))
                .getJieQiTable()[holidayText];
            month = d.getMonth()
            day = d.getDate();
            break;
          } else {
            let temp = item["date"].split(".");
            month = parseInt(temp[0]);
            day = parseInt(temp[1]);
            break;
          }
        }
      }
    }

    if (type=="g") {
      //上午、清晨、中午、傍晚
      let segText = args[0];
      for (let item of this.segs) {
        if (item["seg"].includes(segText)) {
          if (this.accList.indexOf(acc ?? "") < this.accList.indexOf("minute")) {
            acc = "minute";
          }
          if (item["stime"] == "now") {
            hour = moment().hour();
            minute = moment().minute();
            second = moment().second();
            hour1 = moment().hour();
            minute1 = moment().minute();
            second1 = moment().second();
          } else {
            let temp0 = item["stime"].split(":");
            hour = parseInt(temp0[0]);
            minute = parseInt(temp0[1]);
            second = parseInt(temp0[2]);
            let temp1 = item["etime"].split(":");
            hour1 = parseInt(temp1[0]);
            minute1 = parseInt(temp1[1]);
            second1 = parseInt(temp1[2]);
          }
          break;
        }
      }
    }
    
    result["acc"] = acc ?? result["acc"];
    result["metric"] = metric;
    result["year"] = year ?? result["year"];
    result["month"] = month ?? result["month"];
    result["day"] = day ?? result["day"];
    result["hour"] = hour ?? result["hour"];
    result["minute"] = minute ?? result["minute"];
    result["second"] = second ?? result["second"];
    result["year1"] = year1 ?? result["year1"];
    result["month1"] = month1 ?? result["month1"];
    result["day1"] = day1 ?? result["day1"];
    result["hour1"] = hour1 ?? result["hour1"];
    result["minute1"] = minute1 ?? result["minute1"];
    result["second1"] = second1 ?? result["second1"];
    result["isDuration"] = isDuration;

    return result;
  }
  
  parse(text) {
    //首先处理单独的数字形式的日期格式  
    let date
    let newText
    if (text.length==4){  //可能是YYYY
        newText = text
        date = moment(Date.parse(newText))
    }else if (text.length==6){ //可能是YYYYMM
        newText = text.substring(0,4)+"/"+text.substring(4,6)
        date = moment(Date.parse(newText))
    }else if (text.length==8){ //可能是YYYYMMDD
        newText = text.substring(0,4)+"/"+text.substring(4,6)+"/"+text.substring(6,8)
        date = moment(Date.parse(newText))
    }else{
        newText = text
        date = moment(Date.parse(newText))
    }
    if (date && date.isValid()){
      if (newText.length==4)
        return   {"start": date.toDate(), "end": date.toDate(), "acc": "year", "x": null}
      else if (newText.length==7)
        return   {"start": date.toDate(), "end": date.toDate(), "acc": "month", "x": null}
      else if (newText.length==10)
        return   {"start": date.toDate(), "end": date.toDate(), "acc": "day", "x": null}
      else
        return   {"start": date.toDate(), "end": date.toDate(), "acc": "minute", "x": null}
    }
    
    let x = [];
    let dateExps =[ `((上|过去|最近这|最近|近|接下来这|接下来|这|下)个?的?(${this.shuci}+)个?(半)?(${this.cnUnits}))`,
                `((${this.shuci}+)个?(半)?(${this.liangci})(半)?[又]?(${this.shuci}+)?个?(半)?(${this.liangci})?(半)?[之以]?(前|后))`,
                `((${this.shuciA})[-~～至到]?(${this.shuciA})?个?(${this.cnUnits})?)`,
                `(${this.cnDate})`,
                `((${this.cnWeek})(${this.weekci})?)`,
                `(${this.cnHolidays})`,
                `(${this.cnSegs})`,
                `(一?直?[到至\\-~～])`
              ]  
    let dateExp = new RegExp(`(${dateExps.join("|")})`,"g")
    //console.log(text.match(dateExp))
    //console.log("***********************")
    while (true){
      let mt = dateExp.exec(text)
      if (mt==null) break
      //console.log(mt)
      //这个月、近2个月,接下来的两个半月
      if (mt[3] || mt[4] || mt[5] || mt[6]){
        x.push({"type":"a","data":[mt[3],mt[4],mt[5],mt[6]]})
        continue        
      }
      //2个半月前、2小时30分前、3个半月又3天半之前、3周半又3个半天以后
      if (mt[8] || mt[9] || mt[10] || mt[11] || mt[12] || mt[13] || mt[14] || mt[15] || mt[16]){
        x.push({"type":"b","data":[mt[8],mt[9],mt[10],mt[11],mt[12],mt[13],mt[14],mt[15],mt[16]]})
        continue
      }
      // 2点、2021年、3月、3-5点、4～5月份
      let c1 = this.flex2num([mt[19], mt[20], mt[21]]);
      let c2 = this.flex2num([mt[23], mt[24], mt[25]]);
      if (c1 || c2 || mt[26]){
        x.push({"type":"c","data":[c1,c2,mt[26]]})
        continue
      }
      //去年、上个月,昨天、
      if (mt[27]) {
        x.push({"type":"d","data":[mt[27]]})
        continue
      }
      //上个星期5，周三
      if (mt[29] || mt[30]){
        x.push({"type":"e","data":[mt[29],mt[30]]})
        continue
      }
      //春节、中秋、国庆
      if (mt[31]){
        x.push({"type":"f","data":[mt[31]]})
        continue
      }
      //上午、凌晨、傍晚、下午
      if (mt[32]){
        x.push({"type":"g","data":[mt[32]]})
        continue
      }
      //到
      if (mt[33]){
        x.push({"type":"to"})
      }
    }
    let last,start;
    let sDate, eDate;

    //没有匹配返回null
    if (x.length==0) return null

    for (let i = 0; i < x.length; i++) {
      if (x[i].type=="to"){
        // 深拷贝last，避免浅拷贝
        start = JSON.parse(JSON.stringify(last))
        if (last["metric"] == "hour"){
          last["metric"] = null
        }
        continue
      }
      last = this.setDateTime(x[i].type,x[i].data, last);
    }
    if (start == null) {
      //根据last的精度设定sDate，eDate
      let now = moment();
      if (last["isDuration"] ?? false) {
        let temp = moment(
            { "year":last["year"] ?? now.year(),
              "month":last["month"] ?? now.month(),
              "date":last["day"] ?? now.date(),
              "hour":last["hour"] ?? now.hour(),
              "minute":last["minute"]
            });
        if (temp.diff(now)>0) {
          sDate = now.toDate();
          eDate = temp.toDate();
        } else {
          sDate = temp.toDate();
          eDate = now.toDate();
        }
      } else {
        let pDate
        switch (last["acc"]) {
          case "year":
            sDate = moment({"year":last["year"]}).toDate()
            if (last["year1"] != null) {
              eDate = moment({"year":last["year1"]}).toDate();
            } else {
              eDate = this.lastOf(sDate, "year");
            }
            break;
          case "month":
            sDate = moment({"year":last["year"] ?? now.year(),"month": last["month"]})
            pDate = moment(
                {"year":last["year1"] ?? sDate.year(), "month":last["month1"] ?? sDate.month()})
            eDate = this.lastOf(pDate, "month");
            sDate = sDate.toDate()
            break;
          case "day":
            sDate = moment(
                {"year":last["year"] ?? now.year(),
                 "month":last["month"] ?? now.month(),
                 "date": last["day"]})
            pDate = moment(
                {"year":last["year1"] ?? sDate.year(),
                 "month":last["month1"] ?? sDate.month(),
                 "date": last["day1"] ?? sDate.date()
                })
            eDate = this.lastOf(pDate, "day")
            sDate = sDate.toDate()
            break;
          case "hour":
            sDate = moment(
                { "year":last["year"] ?? now.year(),
                  "month":last["month"] ?? now.month(),
                  "date":last["day"] ?? now.date(),
                  "hour":last["hour"]
                })
            pDate = moment(
                { "year":last["year1"] ?? sDate.year(),
                  "month":last["month1"] ?? sDate.month(),
                  "date":last["day1"] ?? sDate.date(),
                  "hour":last["hour1"] ?? sDate.hour()
                });
            eDate = this.lastOf(pDate, "hour");
            sDate = sDate.toDate()
            break;
          case "minute":
            sDate = moment(
                {"year":last["year"] ?? now.year(),
                 "month":last["month"] ?? now.month(),
                 "date":last["day"] ?? now.date(),
                "hour":last["hour"] ?? now.hour(),
                "minute":last["minute"]
                });
            pDate = moment(
                {"year":last["year1"] ?? sDate.year(),
                 "month":last["month1"] ?? sDate.month(),
                 "date":last["day1"] ?? sDate.date(),
                 "hour":last["hour1"] ?? sDate.hour(),
                 "minute":last["minute1"] ?? sDate.minute()
                });
            eDate = this.lastOf(pDate, "minute")
            sDate = sDate.toDate()
            break;
          case "week":
            sDate = moment(
                {"year":last["year"] ?? now.year(),
                 "month":last["month"] ?? now.month(),
                 "date": last["day"] ?? now.date()
                })
            pDate = moment(
                {"year":last["year1"] ?? sDate.year(),
                 "month":last["month1"] ?? sDate.month(),
                 "date": last["day1"] ?? sDate.date()
                })
            eDate = this.lastOf(pDate, "week")
            sDate = sDate.toDate()
            break;
          case "quarter":
            sDate =moment(
                {"year":last["year"] ?? now.year(),
                 "month": last["month"] ?? now.month()})
            pDate = moment(
              {"year":last["year1"] ?? sDate.year(),
               "month":last["month1"] ?? sDate.month()
              });
            eDate = this.lastOf(pDate, "quarter")
            sDate = sDate.toDate()
            break;
          default:
            console.log(last);
        }
      }
    } else {
      //sDate设定start，eDate设定last的精度截止
      let now = moment();
      let temp
      switch (start["acc"]) {
        case "year":
          sDate = moment({"year":start["year"]}).toDate()
          break;
        case "month":
          sDate = moment({"year":start["year"] ?? now.year(), "month":start["month"]}).toDate()
          break;
        case "day":
          sDate = moment({"year":start["year"] ?? now.year(),
              "month":start["month"] ?? now.month(), "date":start["day"]}).toDate()
          break;
        case "hour":
          sDate = moment(
            { "year":start["year"] ?? now.year(),
              "month":start["month"] ?? now.month(),
              "date":start["day"] ?? now.date(),
              "hour":start["hour"]
            }).toDate()
          break;
        case "minute":
          sDate = moment(
            { "year":start["year"] ?? now.year(),
              "month":start["month"] ?? now.month(),
              "date":start["day"] ?? now.date(),
              "hour":start["hour"] ?? now.hour(),
              "minute":start["minute"]
            }).toDate()
          break;
        case "week":
          temp = moment(
              { "year": start["year"] ?? now.year(),
                "month":start["month"] ?? now.month(),
                "date":start["day"] ?? now.date()
              })
          sDate = this.firstOf(temp, "week");
          break;
        case "quarter":
          temp =
              moment({
                  "year":  start["year"] ?? now.year(),
                  "month": start["month"] ?? now.month()
              })
          sDate = this.firstOf(temp, "quarter");
          break;
        default:
          console.log(start);
      }
      switch (last["acc"]) {
        case "year":
          eDate =
              this.lastOf(moment({"year":last["year"] ?? now.year()}), "year")
          break;
        case "month":
          eDate = this.lastOf(
              moment({"year":last["year"] ?? now.year(),"month":last["month"]}), "month")
          break;
        case "day":
          eDate = this.lastOf(
              moment({"year":last["year"] ?? now.year(),"month":last["month"] ?? now.month(),
                  "date":last["day"]}),
              "day")
          break;
        case "hour":
          eDate = this.lastOf(
              moment({"year":last["year"] ?? now.year(), "month":last["month"] ?? now.month(),
                  "date":last["day"] ?? now.date(), "hour":last["hour"]}),
              "hour")
          break;
        case "minute":
          eDate = this.lastOf(
              moment(
                { "year":  last["year"] ?? now.year(),
                  "month": last["month"] ?? now.month(),
                  "date":  last["day"] ?? now.date(),
                  "hour":  last["hour"] ?? now.hour(),
                  "minute": last["minute"]}),
              "minute");
          break;
        case "week":
          temp = moment(
              { "year":last["year"] ?? now.year(),
                "month":last["month"] ?? now.month(),
                "date": last["day"] ?? now.date()
              })
          eDate = this.lastOf(temp, "week");
          break;
        case "quarter":
          temp =
              moment({"year":last["year"] ?? now.year(), "month":last["month"] ?? now.month()})
          eDate = this.lastOf(temp, "quarter");
          break;
        default:
          console.log(last);
      }
    }
    console.log(`start:${sDate}`);
    console.log(`end:${eDate}`);
    return {"start": sDate, "end": eDate, "acc": last["acc"], "x": x}
  }

}

exports.ParseDateTime = ParseDateTime
