/*
  nlpParse = new NlpParseBase()

  nlpParse.parseNumUnit("3.5盘")
  nlpParse.parseNumUnit("2/3大杯")
  nlpParse.parseNumUnit("2个半")

  nlpParse.parseNumUnit("三块5毛六","pay")
  nlpParse.parseNumUnit("2个小时又两刻钟","duration")
  nlpParse.parseNumUnit("500大卡","energy")
  nlpParse.parseNumUnit("2公里零300米","distance")

  nlpParse.parseMetrics("蛋白质") ==>"protein"
  nlpParse.parseMetrics("维生素")  ==>["va","vb",...]

  nlpParse.parseAgg("最大")   ===》max

  nlpParse.parseGroupby("每天") ====》 groupbyDay

  nlpParse.parseCondition("大于200卡") {"operate":">","value":"200卡"}
  nlpParse.parseCondition("介于200到300公斤") {"operate":"between","value1":"200","value2":"300公斤"}
*/
const _ = require('underscore')
class NlpParseBase{   
    constructor(){
        this.version="0.1"
        this.shuci=`[\\d\\.零半正一二两三四五六七八九十百千万]`
        this.shuciA=`(${this.shuci}+)(分之|%|/)?(${this.shuci}*)`
        this.duration=`((${this.shuci}+)[个]?(小时|钟头))?[，。]?((${this.shuci}+)[个]?(分钟|分))?`
        this.formula=`(${this.shuciA})?([增少加减乘除\\+\\-\\*/×÷上去以掉的]+)?(${this.shuciA})?`

        this.unitList=[
            "个",
            "大个",
            "小个",
            "瓶",
            "大瓶",
            "小瓶",
            "杯",
            "大杯",
            "小杯",
            "碗",
            "大碗",
            "小碗",
            "把",
            "大把",
            "小把",
            "盒",
            "大盒",
            "小盒",
            "袋",
            "大袋",
            "小袋",
            "箱",
            "桶",
            "份",
            "大份",
            "小份",
            "盘",
            "大盘",
            "小盘",
            "根",
            "支",
            "包",
            "大包",
            "小包",
            "罐",
            "颗",
            "粒",
            "块",
            "大块",
            "小块",
            "张",
            "勺",
            "大勺",
            "小勺",
            "汤匙",
            "大汤匙",
            "小汤匙",
            "顿",
            "片",
            "毫升",
            "口",
            "大口",
            "小口",
            "次",
            "克",
            "毫升",
            "升",
            "公斤",
            "斤",
            "两",
            "钱",
            "条",
            "尾",
            "只",
            "大只",
            "小只",
            "部",
            "台",
            "场",
            "本",
            "集",
            "百分比",
            "百分数",
            "百分点",
            "个百分比",
            "个百分数",
            "个百分点",
            "公里",
            "米",
            "厘米"
          ]
        this.unit = this.unitList.join("|")
        this.unitConvertMap = {
            "kCal": {"kCal": 1, "kcal": 1, "Kcal": 1, "KJ": 4.18585},
            "千卡": {
              "千卡": 1,
              "大卡": 1,
              "千焦": 4.18585,
              "卡": 1000,
              "KJ": 4.18585,
              "kJ": 4.18585,
              "kcal": 1,
              "Kcal": 1,
              "kCal": 1,
            },
            "g": {"g": 1, "mg": 1000, "ug": 1000000},
            "mg": {"g": 0.001, "mg": 1, "ug": 1000},
            "ug": {"g": 0.000001, "mg": 0.001, "ug": 1, "μg": 1},
            "μg": {"g": 0.000001, "mg": 0.001, "ug": 1, "μg": 1},
            "克": {
              "克": 1,
              "毫克": 1000,
              "微克": 1000000,
              "匙": 10,
              "大匙": 15,
              "小匙": 5,
              "少许": 5,
              "适量": 10,
              "口": 15,
              "小口": 5,
              "大口": 15,
              "斤": 1 / 500,
              "公斤": 1 / 1000,
              "千克": 1 / 1000,
              "两": 1 / 50,
              "钱": 1 / 5
            },
            "ml": {"ml": 1, "l": 0.001},
            "公斤": {
              "公斤": 1,
              "斤": 2,
              "克": 1000,
              "kg": 1,
            },
            "kg": {
              "公斤": 1,
              "斤": 2,
              "克": 1000,
              "kg": 1,
            },
            "毫升": {"毫升": 1, "升": 0.001, "两": 0.5, "斤": 0.05, "公斤": 0.001},
            // "m": {"m": 1, "cm": 100, "mm": 1000, "km": 0.001},
            "厘米": {"厘米": 1, "米": 0.01},
            "米": {
              "米": 1,
              "厘米": 100,
              "毫米": 1000,
              "千米": 0.001,
              "公里": 0.001,
              "里": 0.002,
              "meter": 1,
              "cm": 100,
              "m": 1,
              "尺": 3,
              "寸": 30,
              "英尺": 3.281,
              "英寸": 39.37,
              "步": 1.667
            },
            "m": {
              "米": 1,
              "厘米": 100,
              "毫米": 1000,
              "千米": 0.001,
              "公里": 0.001,
              "里": 0.002,
              "meter": 1,
              "cm": 100,
              "m": 1,
              "尺": 3,
              "寸": 30,
              "英尺": 3.281,
              "英寸": 39.37,
              "步": 1.667
            },
            "meter": {
              "米": 1,
              "厘米": 100,
              "毫米": 1000,
              "千米": 0.001,
              "公里": 0.001,
              "里": 0.002,
              "meter": 1,
              "cm": 100,
              "m": 1,
              "尺": 3,
              "寸": 30,
              "英尺": 3.281,
              "英寸": 39.37,
              "步": 1.667
            },
            "分钟": {
              "分钟": 1,
              "分": 1,
              "秒": 60,
              "秒钟": 60,
              "小时": 1 / 60,
              "钟头": 1 / 60,
              "时辰": 1 / 60,
              "钟": 1 / 60,
              "天": 1 / 60 / 24,
              "seconds": 60,
              "刻": 1 / 15,
              "刻钟": 1 / 15,
              "字": 1 / 5,
              "年": 1 / 60 / 24 / 365,
              "月": 1 / 60 / 24 / 30,
              "季度": 1 / 60 / 24 / 90,
              "周": 1 / 60 / 24 / 7,
              "星期": 1 / 60 / 24 / 7,
              "礼拜": 1 / 60 / 24 / 7,
            },
            "min": {
              "分钟": 1,
              "分": 1,
              "秒": 60,
              "秒钟": 60,
              "小时": 1 / 60,
              "钟头": 1 / 60,
              "钟": 1 / 60,
              "天": 1 / 60 / 24,
              "seconds": 60,
              "刻": 1 / 15,
              "刻钟": 1 / 15,
              "字": 1 / 5,
              "年": 1 / 60 / 24 / 365,
              "月": 1 / 60 / 24 / 30,
              "季度": 1 / 60 / 24 / 90,
              "周": 1 / 60 / 24 / 7,
              "星期": 1 / 60 / 24 / 7,
              "礼拜": 1 / 60 / 24 / 7,
            },
            "小时": {
              "分钟": 60,
              "秒": 3600,
              "小时": 1,
              "钟头": 1,
              "钟": 1,
              "分": 60,
              "秒钟": 3600,
              "天": 1 / 24,
              "刻": 15 / 60,
              "刻钟": 15 / 60,
              "字": 5 / 60
            },
            "元": {"元": 1, "块": 1, "角": 10, "毛": 10, "分": 100},
            "次/分钟": {"次/分钟": 1, "下/分钟": 1, "次": 1, "下": 1},
            "摄氏度": {"摄氏度": 1, "度": 1, "华氏度": 33.8},
            "毫摩尔/升": {"毫摩尔/升": 1, "毫摩尔每升": 1, "mmol/L": 1},
            "毫米汞柱": {"毫米汞柱": 1, "mmHg": 1}
          }
          this.metricMap = {
            //营养素
            "energy": {
              "label": "摄入能量",
              "match": "能量|热量|营养素",
              "enUnit": "kCal",
              "cnUnit": "千卡",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61829",
              "barColor": "ff3366"
            },
            "fat": {
              "label": "脂肪",
              "match": "脂肪|营养(素|成分)",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63191",
              "barColor": "ff99cc"
            },
            "protein": {
              "label": "蛋白质",
              "match": "蛋白[质]?|营养(素|成分)",
              "enUnit": "g",
              "cnUnit": "克",
              "interval": "day",
              "agg": "sum",
              "icon": "fa:63483",
              "barColor": "ccffff"
            },
            "carbohydrate": {
              "label": "碳水化物",
              "match": "碳水(化合?物)?|营养(素|成分)",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61506",
              "barColor": "66ff66"
            },
            "sodium": {
              "label": "盐",
              "match": "盐(份)?|钠|营养(素|成分)",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:62172",
              "barColor": "ffccff"
            },
            //水
            "water": {
              "label": "水",
              "match": "^水(份)?$|开水|饮用水",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61507"
            },
            //膳食纤维
            "dietaryfibre": {
              "label": "膳食纤维",
              "match": "纤维",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61507"
            },
            "sugar": {
              "label": "糖",
              "match": "^糖$|蔗糖",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61507"
            },
            "cholesterol": {
              "label": "胆固醇",
              "match": "胆固醇",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "saturatedFat": {
              "label": "饱和脂肪",
              "match": "饱和脂肪",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
            },
            "mufa": {
              "label": "单不饱和脂肪",
              "match": "单不饱和脂肪",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
            },
            "pufa": {
              "label": "多不饱和脂肪",
              "match": "多不饱和脂肪",
              "enUnit": "g",
              "cnUnit": "克",
              "agg": "sum",
              "interval": "day",
            },
            "cellulose": {
              "label": "纤维素",
              "match": "纤维素",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63367"
            },
            "carotene": {
              "label": "胡萝卜素",
              "match": "胡萝卜素",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63367"
            },
            "alphacarotene": {
              "label": "α-胡萝卜素",
              "match": "胡萝卜素|α胡萝卜素",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63367"
            },
            "betacarotene": {
              "label": "β-胡萝卜素",
              "match": "胡萝卜素|β胡萝卜素",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63367"
            },
            "biotin": {
              "label": "生物素",
              "match": "生物素",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            "choline": {
              "label": "胆碱",
              "match": "胆碱",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            //维生素 ,retinol
            "vitaminA": {
              "label": "维生素A",
              "match": "Va|^维生素A$|^维生素$|视黄素|视黄醇",
              "enUnit": "ug",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "00ff66",
            },
            "thiamin": {
              "label": "维生素B1",
              "match": "Vb1|维生素B1|硫胺素|^维生素$|^维生素B$|B族",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "00ffff",
            },
            "riboflavin": {
              "label": "维生素B2",
              "match": "Vb2|维生素B2|核黄素|^维生素$|^维生素B$|B族",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "33ff77",
            },
            "niacin": {
              "label": "维生素B3",
              "match": "Vb3|维生素B3|烟酸|^维生素$|^维生素B$|B族",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "6699ff",
            },
            "pantothenicacid": {
              "label": "维生素B5",
              "match": "Vb5|维生素B5|泛酸|^维生素$|^维生素B$|B族",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "99ffff",
            },
            "vitaminB6": {
              "label": "维生素B6",
              "match": "Vb6|维生素B6|^维生素$|维生素B$|B族",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "bbffff",
            },
            "folate": {
              "label": "维生素B9",
              "match": "Vb9|维生素B9|叶酸|^维生素$|^维生素B$|B族",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "ddffff",
            },
            "vitaminB12": {
              "label": "维生素B12",
              "match": "Vb12|维生素B12|^维生素$|^维生素B$|B族",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "eeffff",
            },
            "vitaminC": {
              "label": "维生素C",
              "match": "Vc|维生素C|^维生素$",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "ff6633"
            },
            "vitaminD": {
              "label": "维生素D",
              "match": "Vd|维生素D|^维生素$",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "3366ff"
            },
            "vitaminE": {
              "label": "维生素E",
              "match": "Ve|维生素E|^维生素$",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "ffcc33"
            },
            "vitaminK": {
              "label": "维生素K",
              "match": "Vk|维生素K|^维生素$",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
              "icon": "61445",
              "barColor": "990099"
            },
            //微量元素
            "calcium": {
              "label": "钙",
              "match": "钙",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "phosphorus": {
              "label": "磷",
              "match": "磷",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "potassium": {
              "label": "钾",
              "match": "钾",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "manganese": {
              "label": "锰",
              "match": "锰",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "iron": {
              "label": "铁",
              "match": "铁",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            "zinc": {
              "label": "锌",
              "match": "锌",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            "copper": {
              "label": "铜",
              "match": "铜",
              "enUnit": "mg",
              "cnUnit": "毫克",
              "agg": "sum",
              "interval": "day",
            },
            "iodide": {
              "label": "碘",
              "match": "碘",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            "selenium": {
              "label": "硒",
              "match": "硒",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            "magnesium": {
              "label": "镁",
              "match": "镁",
              "enUnit": "ug",
              "cnUnit": "微克",
              "agg": "sum",
              "interval": "day",
            },
            //烟
            "tobacco": {
              "label": "香烟",
              "match": "^烟$|香烟",
              "enUnit": "stick",
              "cnUnit": "支",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:62680"
            },
            //酒
            "beer": {
              "label": "啤酒",
              "match": "啤酒",
              "enUnit": "ml",
              "cnUnit": "毫升",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61692"
            },
            "wine": {
              "label": "红酒",
              "match": "红酒|干红|干白",
              "enUnit": "ml",
              "cnUnit": "毫升",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:62926"
            },
            "baijiu": {
              "label": "白酒",
              "match": "白酒"|"白干",
              "enUnit": "ml",
              "cnUnit": "毫升",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63392"
            },
            "huangjiu": {
              "label": "黄酒",
              "match": "黄酒",
              "enUnit": "ml",
              "cnUnit": "毫升",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:63279"
            },
            //运动
            "sportEnergy": {
              "label": "能量消耗",
              "match": "能量|热量",
              "enUnit": "kCal",
              "cnUnit": "千卡",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:62547",
              "barColor": "aaee0000"
            },
            "sportDuration": {
              "type": "time",
              "label": "时长",
              "match": "时间|久|长时间",
              "enUnit": "min",
              "cnUnit": "分钟",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61463",
              "barColor": "bb00ee00"
            },
            "sportDistance": {
              "type": "distance",
              "label": "距离",
              "match": "距离|远",
              "enUnit": "m",
              "cnUnit": "米",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61464",
              "barColor": "cc0000dd"
            },
            //行为
            "behaviorEnergy": {
              "label": "行为能量",
              "match": "能量|热量",
              "enUnit": "kCal",
              "cnUnit": "千卡",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:62547",
              "barColor": "eeff0000"
            },
            "behaviorDuration": {
              "label": "行为时长",
              "match": "时间|久",
              "enUnit": "min",
              "cnUnit": "分钟",
              "agg": "sum",
              "interval": "day",
              "icon": "fa:61463",
              "barColor": "aa00dd00"
            },
            //花费
            "cost": {
              "label": "开销",
              "match": "花费|支出|支付",
              "enUnit": "¥",
              "cnUnit": "元",
              "agg": "sum",
              "interval": "day",
              "icon": "57522",
              "barColor": "6600ff00"
            },
            //体征
            "weight": {
              "type": "weight",
              "label": "体重",
              "match": "重量|体重",
              "enUnit": "kg",
              "cnUnit": "公斤",
              "agg": "avg",
              "interval": "day",
              "barColor": "200,255,255,128",
              "fontColor": "200,0,0,255",
              "icon": "fa:62614",
              "min": 0,
              "max": 250
            },
            "height": {
              "type": "length",
              "label": "身高",
              "match": "高度|身高",
              "enUnit": "cm",
              "cnUnit": "厘米",
              "agg": "last",
              "interval": "year",
              "icon": "fa:61827"
            },
            "signTemp": {
              "label": "体温",
              "match": "体温|温度",
              "enUnit": "c",
              "cnUnit": "摄氏度",
              "agg": "avg",
              "interval": "day",
              "icon": "fa:62153"
            },
            "signHR": {
              "label": "心率",
              "match": "心率|心跳|脉搏",
              "enUnit": "BPM",
              "cnUnit": "次/分钟",
              "agg": "avg",
              "interval": "day",
              "icon": "57537"
            },
            "signHBP": {
              "label": "收缩压",
              "match": "收缩压|高压|高血压|^血压$",
              "enUnit": "mmHg",
              "cnUnit": "毫米汞柱",
              "agg": "avg",
              "interval": "day",
            },
            "signLBP": {
              "label": "舒张压",
              "match": "舒张压|低压|低血压|^血压$",
              "enUnit": "mmHg",
              "cnUnit": "毫米汞柱",
              "agg": "avg",
              "interval": "day",
            },
            "signBBS": {
              "label": "饭前血糖",
              "match": "^血糖$|饭前血糖|餐前血糖|空腹",
              "enUnit": "mmol/L",
              "cnUnit": "毫摩尔/升",
              "agg": "avg",
              "interval": "day",
            },
            "signABS": {
              "label": "饭后血糖",
              "match": "^血糖$|饭后血糖|餐后血糖",
              "enUnit": "mmol/L",
              "cnUnit": "毫摩尔/升",
              "agg": "avg",
              "interval": "day",
            },
            "signBust": {
              "type": "length",
              "label": "胸围",
              "match": "胸围|三围",
              "enUnit": "cm",
              "cnUnit": "厘米",
              "agg": "last",
              "interval": "month",
            },
            "signWaist": {
              "type": "length",
              "label": "腰围",
              "match": "腰围|三围",
              "enUnit": "cm",
              "cnUnit": "厘米",
              "agg": "last",
              "interval": "month",
            },
            "signHip": {
              "type": "length",
              "label": "臀围",
              "match": "臀围|三围",
              "enUnit": "cm",
              "cnUnit": "厘米",
              "agg": "last",
              "interval": "month",
            },
            "vital": {
              "type": "volume",
              "label": "肺活量",
              "match": "肺活量",
              "enUnit": "ml",
              "cnUnit": "毫升",
              "agg": "last",
              "interval": "month",
            }
        }
        this.aggMap = {
            "avg": {
                "label": "平均",
                "match": "平均"
            },
            "max": { 
                "label": "最大",
                "match": "最大|最多|最高"
            },
            "min": { 
                "label": "最小",
                "match": "最小|最少|最低"
            },
            "sum": { 
                "label": "合计",
                "match": "汇总|总共|总的|合计|总计|一共"
            }
        }
        this.groupbyMap={
            "groupbyHour":{
               "label":"每小时",
               "match":"每个?小时"
            },
            "groupbyDay":{
                "label":"每天",
                "match":"每(天|日)" 
            },
            "groupbyWeek":{
                "label":"每周",
                "match":"每个?(周|星期|礼拜)" 
            },
            "groupbyMonth":{
                "label":"每月",
                "match":"每个?(月|月份)" 
            },
            "groupbyQuarter":{
                "label":"每季度",
                "match":"每个?(季度)" 
            },
            "groupbyYear":{
                "label":"每年",
                "match":"每(年)" 
            }
        }
        this.more = "((>=|>|大于等于|多于等于|多于|超过|大于|高于等于|高于|强于等于|强于)(.+))"
        this.less = "((<=|<|小于等于|少于等于|少于|不足|不到|小于|低于等于|低于|弱于等于|弱于)(.+))"
        this.between = "((在|介于)(.+?)(和|与|-|~|～|到|至)(.+)(之间)?)"
        this.equal = "((==|=|在|是|为|有|等于)(.+))"
        this.more_less = "((在|是|为)?(.+?)(之上|以上|之内|以内|之外|以外|之下|以下))"
        this.the_and = "(且|并且)"
        this.the_or = "(或|或者)"
        this.the_not = "((不|并非|除非|没)?)"
    }
    cn2num(w){  
        if (!w) return null
        if (typeof(w)=="number") w=w.toString()
        var dec=0
        var e = "0123456789零一二两三四五六七八九";
        var decode={"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,
        "零":0,"一":1,"二":2,"两":2,"三":3,"四":4,"五":5,"六":6,"七":7,"八":8,"九":9}  
        var ew = ["十","百","千"];  
        var ej = ["万","亿"];  
        var rss = "^(["+e+ew.join("")+"]+"+ej[1]+")?(["+e+ew.join("")+"]+"+ej[0]+")?(["+e+ew.join("")+"]+)?"+"(点)?"+"(["+e+"]+)?"+"$";  
        //     ^([零一二三四五六七八九十百千]+亿)?([零一二三四五六七八九十百千]+万)?([零一二三四五六七八九十百千]+)?$   
        let numReg=new RegExp("[\\d]+|[\\d]+\\.[\d]+")
        if (w.match(numReg)){
          return parseFloat(w,4)
        }
        if (w=="半") {
          return 0.5
        }
        //console.log("cn2num",w)
        var arr = new RegExp(rss).exec(w);
        //console.log("arr",arr)
        function foh(str){  
            str = new String(str);      
            var a=0;  
            str=str.replace(new RegExp("零","g"),""); 
            if ([0,1,2,3,4,5,6,7,8,9].includes(str)){
               return parseInt(str)
            }
            //if(str.indexOf(ew[0])==0)
            //    a=10;                
            if(new RegExp("(["+e+"])$").test(str))  
                a+=decode[RegExp.$1];  
            if(new RegExp("(["+e+"])?"+ew[0]).test(str))  
                a+=(decode[RegExp.$1]?decode[RegExp.$1]:1)*10;  
            if(new RegExp("(["+e+"])?"+ew[1]).test(str))  
                a+=(decode[RegExp.$1]?decode[RegExp.$1]:1)*100;  
            if(new RegExp("(["+e+"])?"+ew[2]).test(str))  
                a+=(decode[RegExp.$1]?decode[RegExp.$1]:1)*1000;  
            return a;  
        }
        if (arr[4]=="点" && arr[5]){
           dec=parseFloat("0."+arr[5].split("").map(x=>decode[x]).join(""))
        }
        return foh(arr[1])*100000000+foh(arr[2])*10000+foh(arr[3])+dec;  
    }  
    flex2num(strArr){
        //strArr为["三","分之","一"] 或 
        //        ["1","/","3"] 或
        //        ["22.3","%",""] 或 
        //        ["12.345",undefined,""]
        if (!strArr[1]) return this.cn2num(strArr[0])
        if (strArr[1]=="/"){
            return Math.round(this.cn2num(strArr[0])/this.cn2num(strArr[2])*10000)/10000
        }
        if (strArr[1]=="分之"){
            return Math.round(this.cn2num(strArr[2])/this.cn2num(strArr[0])*10000)/10000
        }
        if (strArr[1]=="%"){
            return Math.round(this.cn2num(strArr[0])*100)/10000
        }
    }  
    formula2num(strArr){
        //console.log("formula2num",strArr)
        //strArr为["三","乘以","一"] 或 
        //        ["1","/","3"] 或
        //        ["22.3",undefined,""] 或 
        //        [["12.345","%",""],"+","12"]
        if (!strArr[1]) {
            return Array.isArray(strArr[0])?this.flex2num(strArr[0]):this.cn2num(strArr[0])
        }
        if (["+","加","增加","加上"].includes(strArr[1])){
            let a=Array.isArray(strArr[0])?this.flex2num(strArr[0]):this.cn2num(strArr[0])
            let b
            if (Array.isArray(strArr[2])){
            if (("%","分之").includes(strArr[2][1])){
                b = a*this.flex2num(strArr[2])  
            }else{
                b = this.flex2num(strArr[2])
            }
            }else{
            b = this.cn2num(strArr[2])
            }
            return a+b
        }
        if (["-","减","减少","减去","减掉"].includes(strArr[1])){
            let a=Array.isArray(strArr[0])?this.flex2num(strArr[0]):this.cn2num(strArr[0])
            let b=Array.isArray(strArr[2])?this.flex2num(strArr[2]):this.cn2num(strArr[2])
            return a-b
        }
        if (["*","×","乘","乘上","乘以","的"].includes(strArr[1])){
            let a=Array.isArray(strArr[0])?this.flex2num(strArr[0]):this.cn2num(strArr[0])
            let b=Array.isArray(strArr[2])?this.flex2num(strArr[2]):this.cn2num(strArr[2])
            return a*b
        }
        if (["/","÷","除","除上","除以"].includes(strArr[1])){
            let a=Array.isArray(strArr[0])?this.flex2num(strArr[0]):this.cn2num(strArr[0])
            let b=Array.isArray(strArr[2])?this.flex2num(strArr[2]):this.cn2num(strArr[2])
            return a/b
        }
    }
    parseNumUnit(text,type=null){
        if (type!=null){
            switch (type){
                case "pay":
                    return this.parsePay(text)
                    break
                case "duration":
                    return this.parseDuration(text)
                    break
                case "distance":
                    return this.parseDistance(text)
                    break
                case "energy":
                    return this.parseEnergy(text)
                    break
            }
        }
        let reg = new RegExp(`((${this.formula})个?(半)?(${this.unit}))(又|零)?((${this.formula})个?(半)?(${this.unit})?)?(又|零)?((${this.formula})个?(半)?(${this.unit})?)?((半))?`)
        let match = text.match(reg)
        console.log(match)
        if (match==null){
            return null
        }
        let m,n,o
        //转换第一个公式数据
        m=this.formula2num([[match[4],match[5],match[6]],match[7],[match[9],match[10],match[11]]])
        if (match[12]){
            m+=0.5
        }
        console.log("m",m,match[13])
        //转换第二个公式数据
        n=this.formula2num([[match[18],match[19],match[20]],match[21],[match[23],match[24],match[25]]])
        if (match[26]){
            n+=0.5
        }
        if (match[27]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (n!=0.5){
                n=parseFloat(`0.${n}`)
            }
            match[27]=match[13]
        }
        console.log("n",n,match[27])
        //转换第二个公式数据
        o=this.formula2num([[match[32],match[33],match[34]],match[35],[match[37],match[38],match[39]]])
        if (match[40]){
            o+=0.5
        }
        if (match[41]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (o!=0.5){
                o=parseFloat(`0.${o}`)
            }
            match[41]=match[27]
        }
       console.log("o",o,match[41])
       return {"value":m+n+o,"unit":match[13]}
    }
    parsePay(msg) {
        let pay_liangci = "元|块|角|毛|分"
        let reg = new RegExp(
            `((${this.formula})个?(半)?(${pay_liangci}))(又|零)?((${this.formula})个?(半)?((${pay_liangci}))?)?`);
        let match = msg.match(reg);
        //console.log(match);
        let m,n
        //转换第一个公式数据
        m=this.formula2num([[match[4],match[5],match[6]],match[7],[match[9],match[10],match[11]]])
        if (match[12]){
            m+=0.5
        }
        console.log("m",m,match[13])
        //转换第二个公式数据
        n=this.formula2num([[match[18],match[19],match[20]],match[21],[match[23],match[24],match[25]]])
        if (match[26]){
            n+=0.5
        }
        console.log("n",n,match[27])
        if (match[27]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (n!=0.5){
                n=parseFloat(`0.${n}`)
            }
            match[27]=match[13]
        }
        let test = this.toNormalUnit("pay", m, match[13], n, match[27])
        if (test != null) return test;
        return {"value": m, "unit": match[13]};
    }
    parseDuration(msg) {
        let duration_liangci = "年|月|季度|周|星期|礼拜|天|钟头|钟|小时|时辰|分钟|刻钟|刻|秒钟|分|秒";
        let reg = new RegExp(
            `((${this.formula})个?(半)?(${duration_liangci}))(又|零)?((${this.formula})个?(半)?((${duration_liangci}))?)?`);
        let match = msg.match(reg);
        //console.log(match);
        let m,n
        //转换第一个公式数据
        m=this.formula2num([[match[4],match[5],match[6]],match[7],[match[9],match[10],match[11]]])
        if (match[12]){
            m+=0.5
        }
        //转换第二个公式数据
        n=this.formula2num([[match[18],match[19],match[20]],match[21],[match[23],match[24],match[25]]])
        if (match[26]){
            n+=0.5
        }
        if (match[27]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (n!=0.5){
                n=parseFloat(`0.${n}`)
            }
            match[27]=match[13]
        }
        let test = this.toNormalUnit("duration", m, match[13], n, match[27])
        if (test != null) return test;
        return {"value": m, "unit": match[13]};
    }

    parseEnergy(msg) {
        let energy_liangci = "千卡|大卡|千焦|卡|焦|焦耳|KJ|kJ|kcal|Kcal|kCal"
        let reg = new RegExp(
            `((${this.formula})个?(半)?(${energy_liangci}))(又|零)?((${this.formula})个?(半)?((${energy_liangci}))?)?`);
        let match = msg.match(reg);
        //console.log(match);
        let m,n
        //转换第一个公式数据
        m=this.formula2num([[match[4],match[5],match[6]],match[7],[match[9],match[10],match[11]]])
        if (match[12]){
            m+=0.5
        }
        //转换第二个公式数据
        n=this.formula2num([[match[18],match[19],match[20]],match[21],[match[23],match[24],match[25]]])
        if (match[26]){
            n+=0.5
        }
        if (match[27]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (n!=0.5){
                n=parseFloat(`0.${n}`)
            }
            match[27]=match[13]
        }
        let test = this.toNormalUnit("energy", m, match[13], n, match[27])
        if (test != null) return test;
        return {"value": m, "unit": match[13]};
    }

    parseDistance(msg) {
        let distance_liangci = "公里|里|米|m|km|步";
        let reg = new RegExp(
            `((${this.formula})个?(半)?(${distance_liangci}))(又|零)?((${this.formula})个?(半)?((${distance_liangci}))?)?`);
        let match = msg.match(reg)
        //console.log(match);
        let m,n
        //转换第一个公式数据
        m=this.formula2num([[match[4],match[5],match[6]],match[7],[match[9],match[10],match[11]]])
        if (match[12]){
            m+=0.5
        }
        //转换第二个公式数据
        n=this.formula2num([[match[18],match[19],match[20]],match[21],[match[23],match[24],match[25]]])
        if (match[26]){
            n+=0.5
        }
        if (match[27]==undefined){
            //没有单位则使用上一个单位，数值为小数
            if (n!=0.5){
                n=parseFloat(`0.${n}`)
            }
            match[27]=match[13]
        }
        let test = this.toNormalUnit("length", m, match[13], n, match[27])
        if (test != null) return test
        return {"value": m, "unit": match[13]}
    }

    toNormalUnit(type, one, oneUnit,two=null, twoUnit=null, three=null, threeUnit=null) { 
        let unitMap = {
            "pay": {
            "normal": "元",
            "match": ["元", "块", "角", "毛", "分钱","分"],
            "scale": 1
            },
            "weight": {
            "normal": "克",
            "match": ["斤", "两", "公斤", "克", "钱"],
            "scale": 1
            },
            "length": {
            "normal": "米",
            "match": ["公里", "里", "m", "cm", "米", "厘米", "尺", "寸", "英尺", "英寸", "步"],
            "scale": 1
            },
            "duration": {
            "normal": "分钟",
            "match": ["小时", "分钟", "刻钟", "秒钟"],
            "scale": 0.6
            },
            "energy": {
                "normal": "千卡",
                "match": ["千卡", "大卡", "kcal", "kCal","Kcal"],
                "scale": 1
            },
            "volume": {
            "normal": "毫升",
            "match": ["毫升", "ml", "升"],
            "scale": 1
            }
        }
        let match = unitMap[type]["match"]
        let normal = unitMap[type]["normal"]
        let scale = parseFloat(`${unitMap[type]["scale"]}`)
        if (match.includes(oneUnit)) {
            let s, t, r;
            if (one != null) {
                s = this.convert(normal, one, oneUnit);
                if (two != null) {
                    if (twoUnit != null) {
                        t = this.convert(normal, two, twoUnit);
                    } else {
                        t = this.convert(
                            normal,
                            parseFloat(`0.${two.toString().split('.')[0]}`) / scale,
                            oneUnit);
                    }
                    if (three != null) {
                        if (threeUnit != null) {
                            r = this.convert(normal, three, threeUnit);
                        } else {
                            r = this.convert(
                                normal,
                                parseFloat(`0.${three.toString().split('.')[0]}`) / scale,
                                twoUnit);
                        }
                    }
                }
                return {"value": (s ?? 0) + (t ?? 0) + (r ?? 0), "unit": normal};
            }
        }
        return null;
    }

    convert(baseUnit, fromValue, fromUnit,toUnit=null) {
        if (toUnit == null) toUnit = baseUnit;
        let temp = this.unitConvertMap[baseUnit];
        if (temp==null) return null
        if (temp[fromUnit] != null) {
            if (toUnit == baseUnit) {
                return fromValue / temp[fromUnit];
            } else {
                if (temp[toUnit] != null) {
                    return fromValue / temp[fromUnit] * temp[toUnit];
                } else {
                    return null;
                }
            }
        }
        return null;
    }
    //metric
    parseMetrics(msg){
        let metrics=[]
        for (let key in this.metricMap){
           let reg = RegExp(this.metricMap[key]["match"],"i")
           if (reg.test(msg)){
             metrics.push(key)  
           }   
        }
        if (metrics.length==0){
            return null
        }else{
            return metrics
        } 
    } 
    //agg
    parseAgg(msg){
       for (let key in this.aggMap){
          let reg = RegExp(this.aggMap[key]["match"])
          if (reg.test(msg)){
            return key  
          }   
       }
       return null
    }
    //groupby
    parseGroupby(msg){
        for (let key in this.groupbyMap){
           let reg = RegExp(this.groupbyMap[key]["match"])
           if (reg.test(msg)){
             return key  
           }   
        }
        return null
    }
    //condition
    parseCondition(msg){
        let reg = new RegExp(`${this.the_not}(${this.more}|${this.less}|${this.between}|${this.more_less}|${this.equal})`)
        //console.log(reg)
        let res = reg.exec(msg)
        //console.log(res)
        if (res==null){
            return null
        }
        let isNot = false
        let operate
        let value1,value2
        //isNot？
        if (res[1]){
           isNot = true           
        }else{
           isNot = false
        }
        if (res[4]){
            //isMore?
            if (isNot){
                if (RegExp("等|=").test(res[5])){
                    operate = "<"
                }else{
                    operate = "<="
                }
            }else{
                if (RegExp("等|=").test(res[5])){
                    operate = ">="
                }else{
                    operate = ">"
                }
            }
            value1 = res[6]
        }else if (res[7]){
            //isLess?
            if (isNot){
                if (RegExp("等|=").test(res[8])){
                    operate = ">"
                }else{
                    operate = ">="
                }
            }else{
                if (RegExp("等|=").test(res[8])){
                    operate = "<="
                }else{
                    operate = "<"
                }
            }
            value1 = res[9]
        }else if (res[10]){
            //isBetween?
            if (isNot){
                operate = "not between"
            }else{
                operate = "between"
            }
            value1 = res[12]
            value2 = res[14]
        }else if (res[16]){
            //isEqual? or more or less
            if (isNot){
               if (RegExp("(以内|之内|以下|之下)").test(res[19])){
                   operate = ">"
               }else if (RegExp("(以外|之外|以上|之上)").test(res[19])){
                   operate = "<"
               }else{
                   operate = "!="
               }
            }else{
                if (RegExp("(以内|之内|以下|之下)").test(res[19])){
                   operate = "<"
                }else if (RegExp("(以外|之外|以上|之上)").test(res[19])){
                   operate = ">"
                }else{
                    operate = "="
                }
             }
            value1 = res[18]            
        }else if (res[20]){
            //isEqual? or more or less
            if (isNot){
                operate = "!="
            }else{
                operate = "="
            }
            value1 = res[22]            
        }else {
            return null
        }
        return {"operate":operate,"value1":value1,"value2":value2}
    }    
}

exports.NlpParseBase = NlpParseBase